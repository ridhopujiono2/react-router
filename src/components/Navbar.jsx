import React, { useEffect, useState } from "react";
import Button from "./Button";
import { BookOpen } from "react-feather";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { PATH } from "../config/path";
import toast, { Toaster } from "react-hot-toast";
import { sleep } from "../utils/sleep";
import { Navigate } from "react-router-dom";

function Navbar({ children }) {
  const location = useLocation();
  const navigate = useNavigate();
  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    const token = window.localStorage.getItem("token");
    if (token) {
      setIsLogin(true);
    }
  }, [window.localStorage.getItem("token")]);

  return (
    <div>
      <nav className="flex justify-between items-center bg-slate-100 w-100 h-50 py-4 px-7 text-slate-800">
        <div>
          <Button>
            <BookOpen />
          </Button>
        </div>
        <div>
          <ul className="inline-flex gap-10">
            <li>
              <Link
                className={
                  location.pathname == PATH.home ? "text-emerald-500" : ""
                }
                to={PATH.home}
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                className={
                  location.pathname == PATH.books ||
                  location.pathname == `${PATH.books}/:id`
                    ? "text-emerald-500"
                    : ""
                }
                to={PATH.books}
              >
                Books
              </Link>
            </li>
            {isLogin && (
              <>
                <li>
                  <Link
                    to={PATH.create_book}
                    className="bg-yellow-300 py-1 px-2 rounded"
                  >
                    Publish book
                  </Link>
                </li>
              </>
            )}
          </ul>
        </div>
        <div>
          {!isLogin ? (
            <>
              <Link to={PATH.login} className="mr-3 text-emerald-500">
                Login
              </Link>
              <Link to={PATH.register} className="ml-3">
                Register
              </Link>
            </>
          ) : (
            <>
              <Link
                onClick={() => {
                  window.localStorage.removeItem("token");
                  setIsLogin(false);
                  toast.success("You have successfully logged out.");
                  sleep(2000);
                  navigate("/");
                }}
                className="ml-3 text-red-500"
              >
                Logout
              </Link>
            </>
          )}
        </div>
      </nav>
      {children}
      <Toaster />
    </div>
  );
}

export default Navbar;
