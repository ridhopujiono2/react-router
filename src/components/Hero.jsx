import React from "react";
import { Link } from "react-router-dom";
import { PATH } from "../config/path";
import Button from "./Button";

function Hero() {
  return (
    <div className="flex justify-between items-center h-screen bg-slate-100 w-screen p-20">
      <div className="flex-1">
        <h1 className="text-4xl leading-normal tracking-wide text-slate-700 mb-5">
          Are you looking for books that are trending right now?
        </h1>
        <Link
          to={PATH.books}
          className="bg-emerald-500 text-white rounded border px-4 py-3"
        >
          Get Started !
        </Link>
      </div>
      <div className="flex-1">
        <img src="../../public/hero.png" alt="" />
      </div>
    </div>
  );
}

export default Hero;
