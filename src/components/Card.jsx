import React from "react";
import { Link } from "react-router-dom";
import { PATH } from "../config/path";

function Card(props) {
  return (
    <div className="bg-white rounded-lg border shadow-sm overflow-hidden w-full sm:w-1/2 md:w-1/5 mb-4">
      <Link to={`${PATH.books}/${props.id}`}>
        <img
          className="object-cover w-full h-48"
          src={`http://127.0.0.1:8000/${props.image}`}
          alt="Product"
        />
        <div className="p-4">
          <h2 className="text-lg font-medium text-gray-800 mb-2">
            {props.title}
          </h2>
          <p className="text-sm text-gray-700 mb-4">{props.author}</p>
        </div>
      </Link>
    </div>
  );
}

export default Card;
