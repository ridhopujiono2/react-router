import React from "react";

function Button(props) {
  const addClass = props.addClass ? props.addClass : "";
  return (
    <button
      className={`bg-emerald-500  text-white px-3 py-2 rounded ${addClass}`}
      {...props}
    >
      {props.children}
    </button>
  );
}

export default Button;
