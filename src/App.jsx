import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { PATH } from "./config/path";
import { Helmet } from "react-helmet";
import Navbar from "./components/Navbar";
import HomePage from "./pages/home/HomePage";
import RegisterPage from "./pages/auth/RegisterPage";
import BooksPage from "./pages/home/BooksPage";
import PageNotFound from "./pages/errors/NotFound";
import LoginPage from "./pages/auth/LoginPage";
import CreateBook from "./pages/home/CreateBook";
import BookDetailPage from "./pages/home/BookDetailPage";

function App() {
  return (
    <Router>
      <Helmet>
        <title>Book Store</title>
      </Helmet>
      <Routes>
        <Route
          path={PATH.home}
          element={
            <Navbar>
              <HomePage />
            </Navbar>
          }
        />
        <Route
          path={PATH.books}
          element={
            <Navbar>
              <BooksPage />
            </Navbar>
          }
        />
        <Route
          path={`${PATH.books}/:id`}
          element={
            <Navbar>
              <BookDetailPage />
            </Navbar>
          }
        />
        <Route
          path={PATH.create_book}
          element={
            <Navbar>
              <CreateBook />
            </Navbar>
          }
        />
        <Route path="*" element={<PageNotFound />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
      </Routes>
    </Router>
  );
}

export default App;
