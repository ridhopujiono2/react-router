import React, { useState } from "react";
import { Loader } from "react-feather";
import toast, { Toaster } from "react-hot-toast";
import { createNewBook } from "../../api";
import Button from "../../components/Button";

function CreateBook() {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [publisher, setPublisher] = useState("");
  const [year, setYear] = useState("");
  const [pages, setPages] = useState("");
  const [image, setImage] = useState("");
  const [loading, setLoading] = useState(false);

  const submitHandler = async (e) => {
    e.preventDefault();
    setLoading(true);
    const formData = new FormData(event.target);
    try {
      const store = await createNewBook(formData);
      setTitle("");
      setAuthor("");
      setPublisher("");
      setYear("");
      setPages("");
      setImage("");
      toast.success("Book created successfully", {
        duration: 4000,
      });
      setLoading(false);
    } catch (error) {
      toast.error(error.message, {
        duration: 4000,
      });
    }
  };

  return (
    <div className="p-10 w-1/2">
      <h1 className="text-3xl mb-3">Publish book</h1>
      <form
        onSubmit={(e) => {
          submitHandler(e);
        }}
      >
        <div className="mb-3">
          <input
            type="text"
            className="py-2 px-4 w-full border rounded"
            placeholder="Title"
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            value={title}
            name="title"
            required
          />
        </div>
        <div className="mb-3">
          <input
            type="text"
            className="py-2 px-4 w-full border rounded"
            placeholder="Author"
            onChange={(e) => {
              setAuthor(e.target.value);
            }}
            value={author}
            name="author"
            required
          />
        </div>
        <div className="mb-3">
          <input
            type="text"
            className="py-2 px-4 w-full border rounded"
            placeholder="Publisher"
            onChange={(e) => {
              setPublisher(e.target.value);
            }}
            value={publisher}
            name="publisher"
            required
          />
        </div>
        <div className="mb-3">
          <input
            type="number"
            className="py-2 px-4 w-full border rounded"
            placeholder="Year"
            onChange={(e) => {
              setYear(e.target.value);
            }}
            value={year}
            name="year"
            required
          />
        </div>
        <div className="mb-3">
          <input
            type="number"
            className="py-2 px-4 w-full border rounded"
            placeholder="Pages"
            onChange={(e) => {
              setPages(e.target.value);
            }}
            value={pages}
            name="pages"
            required
          />
        </div>
        <div className="mb-3">
          <input
            type="file"
            className="py-2 px-4 w-full border rounded"
            placeholder="Image"
            onChange={(e) => {
              setImage(e.target.value);
            }}
            value={image}
            name="image"
            required
          />
        </div>
        <div className="flex justify-between">
          <Button>{loading ? <Loader /> : "Publish"}</Button>
        </div>
      </form>

      <Toaster />
    </div>
  );
}

export default CreateBook;
