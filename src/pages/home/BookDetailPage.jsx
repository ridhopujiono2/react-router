import React from "react";
import { useEffect, useState } from "react";
import { Loader } from "react-feather";
import toast, { Toaster } from "react-hot-toast";
import { Link, useNavigate, useParams } from "react-router-dom";
import { deleteBookById, editBookById, getBookDetail } from "../../api";
import Button from "../../components/Button";
import { PATH } from "../../config/path";
import { sleep } from "../../utils/sleep";

function BookDetailPage() {
  const [book, setBook] = useState(null);
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");
  const [publisher, setPublisher] = useState("");
  const [year, setYear] = useState("");
  const [pages, setPages] = useState("");
  const [loading, setLoading] = useState(true);
  const { id } = useParams();
  const [isLogin, setIsLogin] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const detail = async () => {
      try {
        const response = await getBookDetail(id);
        await sleep(1500);
        setBook(response.book);
        setTitle(response.book.title);
        setAuthor(response.book.author);
        setPublisher(response.book.publisher);
        setYear(response.book.year);
        setPages(response.book.pages);
        setLoading(false);
      } catch (e) {
        console.log(e);
      }
    };
    detail();
  }, [id]);

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      const edit = await editBookById(id, {
        title: title,
        author: author,
        publisher: publisher,
        pages: parseInt(pages),
        year: parseInt(year),
      });
      toast.success("Book successfully edited.", {
        duration: 3000,
      });
      await sleep(1500);
      navigate(PATH.books);
    } catch (error) {
      console.log(error);
      toast.error(error.message, {
        duration: 4000,
      });
    }
  };
  const deleteHandler = async (e) => {
    e.preventDefault();
    if (confirm("Are you sure you want to delete this book?")) {
      try {
        const destroy = await deleteBookById(id);
        toast.success("Book successfully deleted", {
          duration: 3000,
        });
        await sleep(3000);
        navigate(PATH.books);
      } catch (error) {
        toast.error(error.message, {
          duration: 4000,
        });
      }
    }
  };

  const openForm = (e) => {
    setIsEdit(true);
    setIsDisabled(false);
  };

  useEffect(() => {
    const token = window.localStorage.getItem("token");
    if (token) {
      setIsLogin(true);
    }
  }, [window.localStorage.getItem("token")]);

  return (
    <div>
      {!loading ? (
        <div className="flex justify-center p-10 gap-10">
          <div className="w-1/2 ">
            <img src={`http://127.0.0.1:8000/${book?.image}`} alt="" />
          </div>
          <div className="flex-1">
            <h1 className="text-3xl mb-3 capitalize">{book?.title}</h1>
            <form
              onSubmit={(e) => {
                submitHandler(e);
              }}
            >
              <div className="mb-3">
                <input
                  type="text"
                  className={`py-2 px-4 w-full border rounded ${
                    isEdit ? "" : "bg-slate-100"
                  }`}
                  placeholder="Title"
                  onChange={(e) => {
                    setTitle(e.target.value);
                  }}
                  value={title}
                  name="title"
                  required
                  disabled={isDisabled}
                />
              </div>
              <div className="mb-3">
                <input
                  type="text"
                  className={`py-2 px-4 w-full border rounded ${
                    isEdit ? "" : "bg-slate-100"
                  }`}
                  placeholder="Author"
                  onChange={(e) => {
                    setAuthor(e.target.value);
                  }}
                  value={author}
                  name="author"
                  required
                  disabled={isDisabled}
                />
              </div>
              <div className="mb-3">
                <input
                  type="text"
                  className={`py-2 px-4 w-full border rounded ${
                    isEdit ? "" : "bg-slate-100"
                  }`}
                  placeholder="Publisher"
                  onChange={(e) => {
                    setPublisher(e.target.value);
                  }}
                  value={publisher}
                  name="publisher"
                  required
                  disabled={isDisabled}
                />
              </div>
              <div className="mb-3">
                <input
                  type="number"
                  className={`py-2 px-4 w-full border rounded ${
                    isEdit ? "" : "bg-slate-100"
                  }`}
                  placeholder="Year"
                  onChange={(e) => {
                    setYear(e.target.value);
                  }}
                  value={year}
                  name="year"
                  required
                  disabled={isDisabled}
                />
              </div>
              <div className="mb-3">
                <input
                  type="number"
                  className={`py-2 px-4 w-full border rounded ${
                    isEdit ? "" : "bg-slate-100"
                  }`}
                  placeholder="Pages"
                  onChange={(e) => {
                    setPages(e.target.value);
                  }}
                  value={pages}
                  name="pages"
                  required
                  disabled={isDisabled}
                />
              </div>
              {isLogin && (
                <>
                  {isEdit ? (
                    <Button type="submit" addClass="mr-2">
                      Simpan
                    </Button>
                  ) : (
                    <button
                      type="button"
                      className="bg-emerald-500 px-3 py-2 rounded text-white mr-2"
                      onClick={openForm}
                    >
                      Edit
                    </button>
                  )}
                  <Button
                    type="button"
                    onClick={deleteHandler}
                    addClass="bg-red-500"
                  >
                    Delete
                  </Button>
                </>
              )}
            </form>
          </div>
        </div>
      ) : (
        <h1 className="text-2xl p-10">Loading ...</h1>
      )}
    </div>
  );
}

export default BookDetailPage;
