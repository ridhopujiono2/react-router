import React, { useEffect, useState } from "react";
import Card from "../../components/Card";
import { getAllBooks } from "../../api";

function BooksPage() {
  const [books, setBooks] = useState(null);

  useEffect(() => {
    const fetchBooks = async () => {
      const { books } = await getAllBooks();
      setBooks(books);
    };

    fetchBooks();
  }, []);
  return (
    <div className="p-10">
      <div className="flex flex-wrap justify-evenly gap-2">
        {books?.length <= 0 ? (
          <>
            <img src="../../../public/empty.jpeg" />
          </>
        ) : (
          ""
        )}
        {books?.map((book, idx) => (
          <Card key={idx} {...book} />
        ))}
      </div>
    </div>
  );
}

export default BooksPage;
