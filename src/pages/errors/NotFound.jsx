import React from "react";

function NotFound() {
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <h1 className="text-3xl text-slate-700 text-center">
        404 | Page Not Found
      </h1>
    </div>
  );
}

export default NotFound;
