import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Button from "../../components/Button";
import { PATH } from "../../config/path";
import toast, { Toaster } from "react-hot-toast";
import { Loader } from "react-feather";
import { registerUser } from "../../api";

function RegisterPage() {
  const [disabled, setDisabled] = useState(true);
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const navigate = useNavigate();

  const checkPassword = (value) => {
    if (value == password) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };
  const submitHandler = async (data) => {
    data.preventDefault();
    setLoading(true);
    try {
      const store = await registerUser({
        name: name,
        email: email,
        password: password,
      });
      if (store) {
        setName("");
        setEmail("");
        setPassword("");
        setConfirmPassword("");
        setLoading(false);
        toast.success("Successfully registered", {
          duration: 4000,
        });
      }
    } catch (error) {
      toast.error(error.message);
      setLoading(false);
    }
  };
  return (
    <div className="flex h-screen items-center justify-center">
      <div className="border w-1/3 p-10 rounded">
        <h1 className="text-2xl mb-5">Register</h1>
        <form
          onSubmit={(e) => {
            submitHandler(e);
          }}
        >
          <div className="mb-3">
            <input
              type="text"
              className="py-2 px-4 w-full border rounded"
              required
              placeholder="Name"
              name="name"
              onChange={(e) => {
                setName(e.target.value);
              }}
              value={name}
            />
          </div>
          <div className="mb-3">
            <input
              type="email"
              className="py-2 px-4 w-full border rounded"
              required
              placeholder="Email"
              name="email"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              value={email}
            />
          </div>
          <div className="mb-3">
            <input
              type="password"
              className="py-2 px-4 w-full border rounded"
              required
              placeholder="Password"
              name="password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              value={password}
            />
          </div>
          <div className="mb-3">
            <input
              type="password"
              className="py-2 px-4 w-full border rounded"
              required
              placeholder="Repeat password"
              name="confirmPassword"
              onChange={(e) => {
                checkPassword(e.target.value);
                setConfirmPassword(e.target.value);
              }}
              value={confirmPassword}
            />
          </div>
          <div className="flex justify-between">
            <Link className="text-emerald-700" to={PATH.login}>
              Already register ?
            </Link>
            <Button
              type="submit"
              disabled={disabled}
              style={{ opacity: disabled ? 0.5 : 1 }}
            >
              {loading ? <Loader /> : "Register"}
            </Button>
          </div>
        </form>
      </div>
      <Toaster />
    </div>
  );
}

export default RegisterPage;
