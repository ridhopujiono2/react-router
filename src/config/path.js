export const API_URL = "http://localhost:8000";

export const PATH = {
  home: "/",
  books: "/books",
  login: "/login",
  register: "/register",
  create_book: "/book/create"
};